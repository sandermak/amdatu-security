/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenstorage.mongo;

import org.amdatu.scheduling.annotations.RepeatForever;
import org.amdatu.scheduling.annotations.RepeatInterval;
import org.osgi.service.log.LogService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@RepeatForever
@RepeatInterval(period = RepeatInterval.DAY, value = 1)
public class MongoTokenPurger implements Job {
	
	// Please keep the 'l' suffices in place, as int type the number will overflow and become negative!
	private static final long TOKEN_PURGE_TIMEOUT = 28l * 24l * 60l * 60l * 1000l; // 4 weeks
	
	private volatile ExpiredTokenPurger m_expiredTokenPurger;
	
	private volatile LogService m_logService;
	
	/** This method is called by the dependency manager when the bundle is started */
    public void start() {
        execute();
    }

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		execute();
	}

    private void execute() {
        m_logService.log(LogService.LOG_INFO, "Purging expired authentication tokens...");
		final int count = m_expiredTokenPurger.purgeTokensIssuedBefore(System.currentTimeMillis() - TOKEN_PURGE_TIMEOUT);
		m_logService.log(LogService.LOG_INFO, "Purged " + count + " tokens");
    }

}
