/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenstorage.mongo;

import org.amdatu.security.tokenprovider.Token;
import org.mongojack.ObjectId;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StoredToken extends Token {

	private String m_id;

	public StoredToken() {
		super(null, null, 0);
	}
	
	public StoredToken(Token token) {
		super(token.getToken(), token.getTokenSecret(), token.getTimestamp());
		this.setProperties(token.getProperties());
	}
	
	public StoredToken(String token, String tokenSecret, long timestamp) {
		super(token, tokenSecret, timestamp);
	}

	@ObjectId
	@JsonProperty("_id")
	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		this.m_id = id;
	}
	
}
