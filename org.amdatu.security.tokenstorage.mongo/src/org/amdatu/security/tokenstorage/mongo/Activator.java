/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenstorage.mongo;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.security.tokenprovider.TokenStorageProvider;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;
import org.quartz.Job;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager)
			throws Exception {
		manager.add(createComponent().setInterface(new String[] {
				TokenStorageProvider.class.getName(),
				ExpiredTokenPurger.class.getName()
			}, null)
			.setImplementation(MongoTokenStorageProvider.class)
			.add(createServiceDependency().setService(MongoDBService.class).setRequired(true))
			.add(createServiceDependency().setService(LogService.class).setRequired(false)));
		
		manager.add(createComponent()
			.setInterface(Job.class.getName(), null)
			.setImplementation(MongoTokenPurger.class)
			.add(createServiceDependency().setService(ExpiredTokenPurger.class).setRequired(true))
			.add(createServiceDependency().setService(LogService.class).setRequired(false)));
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager)
			throws Exception {
	}

}
