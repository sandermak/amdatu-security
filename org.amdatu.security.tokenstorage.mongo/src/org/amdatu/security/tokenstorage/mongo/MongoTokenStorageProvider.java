/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenstorage.mongo;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.security.tokenprovider.Token;
import org.amdatu.security.tokenprovider.TokenStorageProvider;
import org.mongojack.DBQuery;
import org.mongojack.JacksonDBCollection;
import org.osgi.service.log.LogService;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

public class MongoTokenStorageProvider implements TokenStorageProvider, ExpiredTokenPurger {
	
	private static final String TOKEN_COLLECTION = "authtokens";
	
	private volatile MongoDBService m_mongoDBService;
	
	private volatile LogService m_logService;

	public void start() {
		DBCollection collection = m_mongoDBService.getDB().getCollection(TOKEN_COLLECTION);
		collection.ensureIndex(new BasicDBObject("token", 1), "amdatu_token_storage");
		m_logService.log(LogService.LOG_INFO, "Creating authtokens index");
	}
	
	@Override
	public void addToken(Token token) {
		DBCollection coll = m_mongoDBService.getDB().getCollection(TOKEN_COLLECTION);
		JacksonDBCollection<StoredToken, String> tokens = JacksonDBCollection.wrap(coll, StoredToken.class, String.class);
		
		final StoredToken tokenToStore;
		if (token instanceof StoredToken) {
			tokenToStore = (StoredToken) token;
		} else {
			m_logService.log(LogService.LOG_DEBUG, "Converting Token object in StoredToken");
			tokenToStore = new StoredToken(token);
		}
		
		tokens.insert(tokenToStore);
	}

	@Override
	public Token getToken(String token) {
		DBCollection coll = m_mongoDBService.getDB().getCollection(TOKEN_COLLECTION);
		JacksonDBCollection<StoredToken, String> tokens = JacksonDBCollection.wrap(coll, StoredToken.class, String.class);
		return tokens.findOne(new BasicDBObject().append("token", token));
	}

	@Override
	public boolean hasToken(String token) {
		return getToken(token) != null;
	}

	@Override
	public void removeToken(Token token) {
		DBCollection coll = m_mongoDBService.getDB().getCollection(TOKEN_COLLECTION);
		JacksonDBCollection<StoredToken, String> tokens = JacksonDBCollection.wrap(coll, StoredToken.class, String.class);
		tokens.remove(new BasicDBObject().append("token", token.getToken()));
	}

    @Override
    public void updateToken(Token token) {
        
    }

	@Override
	public int purgeTokensIssuedBefore(long timestamp) {
		DBCollection coll = m_mongoDBService.getDB().getCollection(TOKEN_COLLECTION);
		JacksonDBCollection<StoredToken, String> tokens = JacksonDBCollection.wrap(coll, StoredToken.class, String.class);
		return tokens.remove(DBQuery.lessThan("timestamp", timestamp)).getN();
	}

}
