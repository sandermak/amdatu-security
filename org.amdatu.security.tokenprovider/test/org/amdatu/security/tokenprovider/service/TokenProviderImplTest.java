package org.amdatu.security.tokenprovider.service;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.amdatu.security.tokenprovider.Token;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenStorageProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TokenProviderImplTest {

	@InjectMocks
	private TokenProvider tokenProvider = new TokenProviderImpl();
	
	@Mock
	private TokenStorageProvider tokenStorageProviderMock;
	
	@Test
	public void testInvalidateExistingToken() throws Exception {
		Token token = new Token("foo", "bar", 1);
		when(tokenStorageProviderMock.hasToken(anyString())).thenReturn(true);
		when(tokenStorageProviderMock.getToken(anyString())).thenReturn(token);

		tokenProvider.invalidateToken("mytoken");

		verify(tokenStorageProviderMock).removeToken(token);
	}

	@Test
	public void testInvalidateNonExistingToken() throws Exception {
		when(tokenStorageProviderMock.hasToken(anyString())).thenReturn(false);
		when(tokenStorageProviderMock.getToken(anyString())).thenReturn(null);

		tokenProvider.invalidateToken("mytoken");

		verify(tokenStorageProviderMock, never()).removeToken(null);
	}
}
