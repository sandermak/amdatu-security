package org.amdatu.security.tokenprovider.tests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;

import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;

import org.amdatu.bndtools.test.BaseOSGiServiceTest;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.TokenStorageProvider;

public class TokenProviderTest extends BaseOSGiServiceTest<TokenProvider> {
	private volatile TokenProvider m_TokenProvider;
	
    public TokenProviderTest() {
		super(TokenProvider.class);
	}
    
    @Override
    public void setUp() throws Exception {
    	Properties props = new Properties();
    	props.put("secretkey", "[randomkey]");
    	configure(TokenProvider.PID, props);
    	TokenStorageProvider tokenStorageProviderMock = mock(TokenStorageProvider.class);
    	when(tokenStorageProviderMock.hasToken(anyString())).thenReturn(true);
    	context.registerService(TokenStorageProvider.class.getName(), tokenStorageProviderMock, null);
    	
    	addServiceDependencies(TokenProvider.class);
    	super.setUp();
    }

    public void testGenerateToken() throws Exception {
    	
    	SortedMap<String, String> attributes = new TreeMap<>();
    	attributes.put(TokenProvider.USERNAME, "testuser");
    	attributes.put("someotherkey", "somevalue");
    	
    	String token = m_TokenProvider.generateToken(attributes);
    	
    	SortedMap<String, String> verified = m_TokenProvider.verifyToken(token);
    	assertEquals(verified.get(TokenProvider.USERNAME), "testuser");
    	assertEquals(verified.get("someotherkey"), "somevalue");
    }
}
